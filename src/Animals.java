import javax.lang.model.element.Name;

/**
 * Created by Макс on 21.10.2014.
 */
public abstract class Animals {
public  static final String OUTPUT_FORMAT_LINE
        = "%s говорит '%s'.";

    private String name;
    private String voice;

    protected Animals (String name, String voice){
        this.name=name;
        this.voice=voice;

    }
    public  String getName() {return name;}


    public  String getVoice() {return voice;}


    public void printDisplay() {
        System.out.print(String.format(
                OUTPUT_FORMAT_LINE,
                name,
                voice
        ));
    }


}
