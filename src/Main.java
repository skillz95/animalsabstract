/**
 * Created by Макс on 23.10.2014.
 */
public class Main {
    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog ("Тузик", "Тяф-тяф");
        Cat cat = new Cat("Кошка","Мяу-Мяу");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();
        cat.printDisplay();
        }


}
